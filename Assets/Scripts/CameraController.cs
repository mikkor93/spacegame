﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public float scrollSpeed = 5f;
    public float cameraMax = 15;
    public float cameraMin = 1;

    Camera camera;


    void Start() {
        camera = GetComponent<Camera>();
    }


	void Update () {
        //Zoom out
        if(Input.GetAxis("Mouse ScrollWheel") < 0 && camera.orthographicSize < cameraMax) {
            camera.orthographicSize = Mathf.Max(camera.orthographicSize +1);
        }
        //Zoom in
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && camera.orthographicSize > cameraMin) {
            camera.orthographicSize = Mathf.Min(camera.orthographicSize - 1);
        }
	}
}
