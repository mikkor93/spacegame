﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//This is for editor
[System.Serializable]
public class Mission {
    public string name;
    public int xpReward;
    public float durationInSeconds;
    public bool available = false;
    public Button.ButtonClickedEvent onClick;
}

public class MissionHandler : MonoBehaviour {

    public Transform parentWindow;
    public GameObject sampleButton;
    public List<Mission> misions;
    public List<GameObject> availableMissions;
    public List<float> missionTimers;

    public Text T_MissionDuration;
    [SerializeField]
    private float selectedMissionDurationSeconds;


    void Start() {
        PopulateList();
    }

    void PopulateList() {
        foreach(var mission in misions) {
            GameObject newButton = Instantiate(sampleButton) as GameObject;
            SampleButton button = newButton.GetComponent<SampleButton>();
            button.name = mission.name;
            button.xpReward = mission.xpReward;
            button.durationInSeconds = mission.durationInSeconds;
            button.available = mission.available;
            button.button.onClick = mission.onClick;

            //Parent under scrollview and make sure its scale is 1
            newButton.transform.SetParent(parentWindow);
            newButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            availableMissions.Add(newButton);   
            //TODO only show available missions
        }
    }

    void RePopulateList() {
        foreach(var mission in availableMissions) {

        }
    }

    void Update() {
        //TODO move this whole schebang to iterator for more optimized performance
        for(int i = 0; i < missionTimers.Count; i++) {
            if (missionTimers[i] > 0) {
                missionTimers[i] -= Time.deltaTime;
            }

            //Clear done mission timers
            //TODO move this to where accepting completed missions
            else if(missionTimers[i] <= 0) {
                missionTimers.Remove(missionTimers[i]);
            }
        }
    }

    public void Mission(int missionDuration) {
        //TODO remove selected mission from avalableMissions list
        selectedMissionDurationSeconds = missionDuration;
        missionTimers.Add(missionDuration);
        T_MissionDuration.text = "Current mission duration: " + selectedMissionDurationSeconds;
    }
}
