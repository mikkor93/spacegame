﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerShip : MonoBehaviour {

    public int health;
    public int missionPoints;
    public int cargoholdSize;

    public float movementSpeed = 5f;

    Vector3 newPos;

    LineRenderer moveLine;
    public LineRenderer fireLine;
    bool showFireLine = false;


    void Start() {
        moveLine = GetComponent<LineRenderer>();
    }

    void Update () {
        //Movement
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit)) {
                newPos = new Vector3(hit.point.x, hit.point.y, 0);  
            }
        }

        //TODO make this non linear
        //TODO also add lerp for rotation
        transform.position = Vector3.MoveTowards(transform.position, newPos, Time.deltaTime * movementSpeed);

        //Move line
        moveLine.SetPosition(0, transform.position);
        moveLine.SetPosition(1, newPos);

        //Shooting
        if (Input.GetMouseButtonDown(0)) {
            showFireLine = true;
            DrawFireLine();
        }
    }

    void DrawFireLine() {
        fireLine.enabled = true;

        fireLine.SetPosition(0, fireLine.transform.position);
        fireLine.SetPosition(0, newPos);
    }
}
