﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SampleButton : MonoBehaviour {
    public Button button;
    public string name;
    public int xpReward;
    public float durationInSeconds;
    public bool available = false;
}
